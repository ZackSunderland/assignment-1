//
//  ViewController.m
//  Assignment 1
//
//  Created by Zackary Sunderland on 1/15/15.
//  Copyright (c) 2015 Zackary Sunderland. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIScrollView* sv = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sv.backgroundColor = [UIColor blackColor];
    
    [sv setContentSize:CGSizeMake(self.view.frame.size.width*3, self.view.frame.size.height*3)];
    [self.view addSubview:sv];
    
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            UIView* view = [[UIView alloc]initWithFrame:CGRectMake(i * self.view.frame.size.width, j * self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
            UILabel* label = [[UILabel alloc]initWithFrame:CGRectMake(((i + .5) * self.view.frame.size.width) - 5, ((j + .5) * self.view.frame.size.height - 10), 10, 20)];
           
            if (i == 0 && j == 0) {
                view.backgroundColor = [UIColor redColor];
                label.text = @"1";
            } else if (i == 1 && j == 0) {
                view.backgroundColor = [UIColor yellowColor];
                label.text = @"2";
            } else if (i == 2 && j == 0) {
                view.backgroundColor = [UIColor purpleColor];
                label.text = @"3";
            } else if (i == 0 && j == 1) {
                view.backgroundColor = [UIColor blueColor];
                label.text = @"4";
            } else if (i == 1 && j == 1) {
                view.backgroundColor = [UIColor greenColor];
                label.text = @"5";
            } else if (i == 2 && j == 1) {
                view.backgroundColor = [UIColor whiteColor];
                label.text = @"6";
            } else if (i == 0 && j == 2) {
                view.backgroundColor = [UIColor cyanColor];
                label.text = @"7";
            } else if (i == 1 && j == 2) {
                view.backgroundColor = [UIColor grayColor];
                label.text = @"8";
            } else if (i == 2 && j == 2) {
                view.backgroundColor = [UIColor brownColor];
                label.text = @"9";
            }
        
            [sv addSubview:view];
            [sv addSubview:label];
        }
    }
    
    [sv setPagingEnabled: YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
