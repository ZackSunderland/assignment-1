//
//  AppDelegate.h
//  Assignment 1
//
//  Created by Zackary Sunderland on 1/15/15.
//  Copyright (c) 2015 Zackary Sunderland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

