//
//  main.m
//  Assignment 1
//
//  Created by Zackary Sunderland on 1/15/15.
//  Copyright (c) 2015 Zackary Sunderland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
